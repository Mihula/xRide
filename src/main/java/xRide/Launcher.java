package xRide;

import xRide.gfx.Assets;

public class Launcher {
    public static void main(String[] args) {
        try {
            Assets.init();
            new Game().start();
        } catch (Exception e) {
            System.out.println("Assets were not loaded successfully!");
        }
    }
}

package xRide.entities;

import java.awt.*;

public abstract class Entity {
    protected float x;
    protected float y;

    public Entity() {
    }

    public Entity(float x, float y) {
        this.x = x;
        this.y = y;
    }

    public abstract void tick();

    public abstract void render(Graphics g);
}

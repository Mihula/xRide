package xRide.entities;

import xRide.Properties;
import xRide.gfx.Assets;

import java.awt.*;

public class GameBackground extends Entity {
    private int imagePosition = 0;

    public void tick() {
        if (imagePosition + Properties.SPEED >= Properties.HEIGHT) {
            imagePosition = 0;
        }
        imagePosition += Properties.SPEED;
    }

    public void render(Graphics g) {
        g.drawImage(Assets.background, 0, imagePosition, null);
        g.drawImage(Assets.background, 0, imagePosition - Properties.HEIGHT, null);
    }
}

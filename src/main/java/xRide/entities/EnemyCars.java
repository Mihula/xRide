package xRide.entities;

import xRide.Properties;
import xRide.gfx.Assets;
import xRide.util.PositionGenerator;

import java.awt.*;

public class EnemyCars extends Entity {
    private static final int GAP = 180;
    private static final int CAR_HEIGHT = 167;

    private int coordinates[];
    private int positions[];

    public EnemyCars() {
        super();
        coordinates = new int[]{-2 * CAR_HEIGHT, -(GAP + 3 * CAR_HEIGHT), -(2 * GAP + 4 * CAR_HEIGHT)};
        positions = new int[]{PositionGenerator.generatePosition(), PositionGenerator.generatePosition(),
                PositionGenerator.generatePosition()};
    }

    @Override
    public void tick() {
        if (coordinates[0] >= Properties.HEIGHT && coordinates[2] >= GAP) {
            coordinates[0] = -CAR_HEIGHT;
            positions[0] = PositionGenerator.generatePosition();
        }

        if (coordinates[1] >= Properties.HEIGHT && coordinates[0] >= GAP) {
            coordinates[1] = -CAR_HEIGHT;
            positions[1] = PositionGenerator.generatePosition();
        }

        if (coordinates[2] >= Properties.HEIGHT && coordinates[1] >= GAP) {
            coordinates[2] = -CAR_HEIGHT;
            positions[2] = PositionGenerator.generatePosition();
        }
        coordinates[0] += Properties.SPEED;
        coordinates[1] += Properties.SPEED;
        coordinates[2] += Properties.SPEED;
    }

    @Override
    public void render(Graphics g) {
        g.drawImage(Assets.enemyCar, positions[0], coordinates[0], null);
        g.drawImage(Assets.enemyCar, positions[1], coordinates[1], null);
        g.drawImage(Assets.enemyCar, positions[2], coordinates[2], null);
    }

    public boolean crashed(int x) {
        if (x == positions[0] && (coordinates[0] > 293 && coordinates[0] < 628)) {
            return true;
        }
        if (x == positions[1] && (coordinates[1] > 293 && coordinates[1] < 628)) {
            return true;
        }
        if (x == positions[2] && (coordinates[2] > 293 && coordinates[2] < 628)) {
            return true;
        }
        return false;
    }
}

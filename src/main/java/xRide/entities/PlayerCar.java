package xRide.entities;

import xRide.Properties;
import xRide.gfx.Assets;
import xRide.input.KeyManager;

import java.awt.*;

public class PlayerCar extends Entity {
    private boolean movingLeft = false;
    private boolean movingRight = false;
    private KeyManager keyManager;

    public PlayerCar(KeyManager keyManager, float x, float y) {
        super(x, y);
        this.keyManager = keyManager;
    }

    @Override
    public void tick() {
        if (movingLeft) {
            if (x - Properties.SPEED <= Properties.LEFT_POSITION) {
                x = Properties.LEFT_POSITION;
                movingLeft = false;
            } else {
                x -= Properties.SPEED;
            }
            return;
        }

        if (movingRight) {
            if (x + Properties.SPEED >= Properties.RIGHT_POSITION) {
                x = Properties.RIGHT_POSITION;
                movingRight = false;
            } else {
                x += Properties.SPEED;
            }
            return;
        }

        if (keyManager.isLeft()) {
            if (x != Properties.LEFT_POSITION) {
                movingLeft = true;
                x -= Properties.SPEED;
            }
        }

        if (keyManager.isRight()) {
            if (x != Properties.RIGHT_POSITION) {
                movingRight = true;
                x += Properties.SPEED;
            }
        }
    }

    @Override
    public void render(Graphics g) {
        g.drawImage(Assets.playerCar, (int) x, (int) y, null);
    }

    public int getCoordinateX() {
        return (int) x;
    }
}

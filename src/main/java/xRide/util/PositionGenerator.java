package xRide.util;

import java.util.Random;

import xRide.Properties;

public class PositionGenerator {
	private static Random random = new Random();

	public static int generatePosition() {
		return random.nextBoolean() ? Properties.RIGHT_POSITION : Properties.LEFT_POSITION;
	}
}

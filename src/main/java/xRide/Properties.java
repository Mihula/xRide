package xRide;

import java.awt.*;

public final class Properties {
    public static final int FPS = 60;
    public static final int SPEED = 19;
    public static final int WIDTH = 480;
    public static final int HEIGHT = 640;
    public static final int LEFT_POSITION = 112;
    public static final int RIGHT_POSITION = 269;
    public static final String TITLE = "xRide";
    public static final Color COLOR = new Color(112, 20, 20, 255);
}

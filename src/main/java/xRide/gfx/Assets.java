package xRide.gfx;

import javax.imageio.ImageIO;
import java.awt.image.BufferedImage;

public final class Assets {
    public static BufferedImage background;
    public static BufferedImage playerCar;
    public static BufferedImage enemyCar;
    public static BufferedImage endScreen;
    public static BufferedImage startScreen;

    public static void init() throws Exception {
        background = loadImage("/background.png");
        playerCar = loadImage("/playerCar.png");
        enemyCar = loadImage("/enemyCar.png");
        endScreen = loadImage("/endScreen.png");
        startScreen = loadImage("/startScreen.png");
    }

    private static BufferedImage loadImage(String path) throws Exception {
        return ImageIO.read(Assets.class.getResource(path));
    }
}

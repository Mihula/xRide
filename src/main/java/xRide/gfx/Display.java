package xRide.gfx;

import xRide.Properties;

import javax.swing.*;
import java.awt.*;

public final class Display {
    private final JFrame frame;
    private final Canvas canvas;

    public Display() {
        frame = initFrame();
        canvas = initCanvas();
        frame.add(canvas);
        frame.pack();
    }

    private JFrame initFrame() {
        JFrame frame = new JFrame();
        frame.setTitle(Properties.TITLE);
        frame.setSize(Properties.WIDTH, Properties.HEIGHT);
        frame.setDefaultCloseOperation(WindowConstants.EXIT_ON_CLOSE);
        frame.setResizable(false);
        frame.setLocationRelativeTo(null);
        frame.setVisible(true);
        return frame;
    }

    private Canvas initCanvas() {
        Canvas canvas = new Canvas();
        canvas.setPreferredSize(new Dimension(Properties.WIDTH, Properties.HEIGHT));
        canvas.setMinimumSize(new Dimension(Properties.WIDTH, Properties.HEIGHT));
        canvas.setMaximumSize(new Dimension(Properties.WIDTH, Properties.HEIGHT));
        canvas.setFocusable(false);
        return canvas;
    }

    public Canvas getCanvas() {
        return canvas;
    }

    public JFrame getFrame() {
        return frame;
    }
}

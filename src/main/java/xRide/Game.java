package xRide;

import xRide.gfx.Display;
import xRide.input.KeyManager;
import xRide.states.EndState;
import xRide.states.GameState;
import xRide.states.StartState;
import xRide.states.State;

import java.awt.*;
import java.awt.image.BufferStrategy;

public class Game implements Runnable {
    private static final int NANOSECONDS = 1000000000;
    private static final double TIME_PER_TICK = NANOSECONDS / Properties.FPS;

    private int score;
    private BufferStrategy bufferStrategy;
    private KeyManager keyManager;
    private State state;
    private State endState = new EndState(this);

    synchronized void start() {
        Thread thread = new Thread(this);
        thread.start();
    }

    public void run() {
        init();

        double lag = 0;
        long currentTime;
        long previousTime = System.nanoTime();
        while (true) {
            currentTime = System.nanoTime();
            lag += currentTime - previousTime;
            previousTime = currentTime;

            while (lag >= TIME_PER_TICK) {
                input();
                tick();
                render();
                lag -= TIME_PER_TICK;
            }
            try {
                Thread.sleep(0, 100000);
            } catch (InterruptedException ignored) {
            }
        }
    }

    private void init() {
        Display display = new Display();
        keyManager = new KeyManager();
        display.getFrame().addKeyListener(keyManager);
        display.getCanvas().createBufferStrategy(3);
        bufferStrategy = display.getCanvas().getBufferStrategy();
        state = new StartState(this);
    }

    private void tick() {
        state.tick();
    }

    private void input() {
        keyManager.tick();
    }

    private void render() {
        Graphics graphics = bufferStrategy.getDrawGraphics();
        graphics.clearRect(0, 0, Properties.WIDTH, Properties.HEIGHT);
        state.render(graphics);
        bufferStrategy.show();
        graphics.dispose();
    }

    public KeyManager getKeyManager() {
        return keyManager;
    }

    public void startGame() {
        state = new GameState(this);
    }

    public void endGame(int score) {
        this.score = score;
        state = endState;
    }

    public int getCalculatedScore() {
        return (score / 10) * 5;
    }
}

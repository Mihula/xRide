package xRide.states;

import xRide.Game;
import xRide.gfx.Assets;

import java.awt.*;

public class StartState extends State {
	private final String name = System.getProperty("user.name") + "!";
	private final Font font = new Font("Verdana", Font.BOLD, 36);

	public StartState(Game game) {
		super(game);
	}

	@Override
	public void tick() {
		if (game.getKeyManager().isSpace()) {
			game.startGame();
		}
	}

	@Override
	public void render(Graphics g) {
		g.drawImage(Assets.startScreen, 0, 0, null);
		g.setFont(font);
		drawText(g, name);
	}
}
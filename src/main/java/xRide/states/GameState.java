package xRide.states;

import xRide.Game;
import xRide.entities.EnemyCars;
import xRide.entities.GameBackground;
import xRide.entities.PlayerCar;
import xRide.util.PositionGenerator;

import java.awt.*;

public class GameState extends State {
    private static final int START_POSITION = 460;

    private int score = 0;
    private final PlayerCar playerCar;
    private final GameBackground gameBackground;
    private final EnemyCars enemyCars;

    public GameState(Game game) {
        super(game);
        playerCar = new PlayerCar(game.getKeyManager(), PositionGenerator.generatePosition(), START_POSITION);
        gameBackground = new GameBackground();
        enemyCars = new EnemyCars();
    }

    @Override
    public void tick() {
        if (enemyCars.crashed(playerCar.getCoordinateX())) {
            game.endGame(score);
            return;
        }
        gameBackground.tick();
        enemyCars.tick();
        playerCar.tick();
        score++;
    }

    @Override
    public void render(Graphics g) {
        gameBackground.render(g);
        enemyCars.render(g);
        playerCar.render(g);
    }
}

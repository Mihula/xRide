package xRide.states;

import xRide.Game;
import xRide.gfx.Assets;

import java.awt.*;

public class EndState extends State {
    private final Font font = new Font("Verdana", Font.BOLD, 24);

    public EndState(Game game) {
        super(game);
    }

    @Override
    public void tick() {
        if (game.getKeyManager().isSpace()) {
            game.startGame();
        }
    }

    @Override
    public void render(Graphics g) {
        g.drawImage(Assets.endScreen, 0, 0, null);
        g.setFont(font);
        int score = game.getCalculatedScore();
        StringBuilder result = new StringBuilder("your score is ");
        result.append(score);
        while(score >= 1500) {
            result.append("!");
            score -= 500;
        }
        drawText(g, result.toString());
    }
}
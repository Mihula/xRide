package xRide.states;

import xRide.Game;
import xRide.Properties;

import java.awt.*;

public abstract class State {
    private static final int POSITION = 240;

    protected Game game;

    public State(Game game) {
        this.game = game;
    }

    public abstract void tick();

    public abstract void render(Graphics g);

    protected void drawText(Graphics g, String text) {
        g.setColor(Properties.COLOR);
        int width = g.getFontMetrics().stringWidth(text);
        g.drawString(text, POSITION - width / 2, POSITION);
    }
}
